<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo; section>
    <#if section = "title">
        ${msg("loginTitle",(realm.displayName!''))}
    <#elseif section = "header">
        <script>
            $(function() {                
                $("input").on('focusout', function(){
                    $(this).each(function(i, e){
                        if($(e).val() != ""){
                            $(e).addClass('not-empty');
                        }else{
                            $(e).removeClass('not-empty');
                        }
                    });
                });
            });
        </script>
    <#elseif section = "form">        
        <div class="form-title">Sign In</div>
            <#if realm.password>
                <form id="kc-form-login" class="form" onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post">
                    <div class="input-field">
                        <input id="email" type="text" name="username" tabindex="1" autocomplete="off">
                        <i class="material-icons">email</i>
                        <label for="email">${msg("username")}</label>
                    </div>
                    <div class="input-field">
                        <input id="password" type="password" name="password" tabindex="2" autocomplete="off">
                        <i class="material-icons">lock</i>
                        <label for="password">${msg("password")}</label>
                    </div>
                    <div class="button">
                        <button class="login">Login</button>
                    </div>
                     <div class="button">
                        <#if realm.resetPasswordAllowed>
                            <a class="forgot-pw" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a>
                        </#if>
                    </div>
                </form>
            </#if>
            <#if social.providers??>
                <div id="socialLinks">
                    <hr />
                    <p class="para">or sign in with</p>
                    <ul>
                        <#list social.providers as p>
                            <li>
                                <a href="${p.loginUrl}" id="social-${p.alias}" class="${p.providerId}"><span><i class="fab fa-${p.providerId}"></i></span>${p.displayName}</span></a>
                            </li>
                        </#list>
                    </ul>
                </div>
            </#if>
        </div>            
    </#if>
</@layout.registrationLayout>

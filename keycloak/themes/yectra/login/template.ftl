<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayWide=false>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><#nested "title"></title>
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <link href="https://www.yectra.com/favicon.ico" rel="icon"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/icon?family=Roboto" rel="stylesheet"/>
    <script src="https://kit.fontawesome.com/debfde7b5b.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <#nested "header">
</head>

<body>
    <div id="signin">
        <#if displayMessage && message?has_content> 
            <div class="alert alert-${message.type}">
                <#if message.type = 'success'><i class="fas fa-check-circle"></i></#if>
                <#if message.type = 'warning'><i class="fas fa-exclamation-circle"></i></#if>
                <#if message.type = 'error'><i class="fas fa-times-circle"></i></#if>
                <#if message.type = 'info'><i class="fas fa-info-circle"></i></#if>
                <span>${message.summary?no_esc}</span>
            </div>
        </#if>
        <#nested "form">
    </div> 
</body>
</html>
</#macro>

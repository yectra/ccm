import { BaseModel, DataRequest } from './base.model';
import { ContentModel } from './content.model';

export class PageSection {
    public name: string;
    public contents: Array<ContentModel>=[];
    public content: Array<contentObject>;
}

export class contentObject {
    public id: ContentModel;
}

export class PageModel extends BaseModel {
    public id: number;
    public siteId: number;
    public name: string;
    public path: string;
    public status: string;
    public pageLayoutId: string;
    public pageSections: Array<PageSection> = [];
    
    public createdBy: string;
    public createdDate: string;
    public modifiedBy: string;
    public modifiedDate: string;
}

export class PageRequest extends DataRequest {
    search: string;
}

import { BaseModel, DataRequest } from './base.model'

export class AttributeModel {
    id:string;
    category:String;
    items: Array<Attributes> = [];
}

export class Attributes{
    public  name:string;
    public type:string;
    public enum:string;
}


export class AttributeRequest extends DataRequest {
    search: string;
}

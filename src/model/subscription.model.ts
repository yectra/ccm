import { BaseModel, DataRequest } from './base.model'

export class SubscriptionModel {
    id:string;
    plan_id: string;
    total_count:number;
    quantity:number;
    start_at: number;
    expire_by: number;
    customer_notify: boolean;
    status:string;
    short_url:string;
    customer_id:string;
    items: Array<InvoiceItems> = [];
    customer_id_List: Array<String>= [];
  
   
}

export class InvoiceItems {

    public  id:String;
	public  customer_id:String;
	public  order_id:String;
	public  subscription_id:String;
	public  payment_id:String;
	public  status:String;
	public  paid_at:number;
    public customer_details:CustomerDetails;
    public line_items: Array<AmountDetails> = [];
    public short_url:String;

}

export class AmountDetails{
    public  amount:number;
    public name:string;
}

export class SubscriptionNotes{
    public  siteId:string;
    public siteURl:string;
}

export class  CustomerDetails {

    public  id:String;
	public  name:String;
	public  email:String;
	public  contact:String;

}

export class SubscriptionRequest extends DataRequest {
    search: string;
}

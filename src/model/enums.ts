export enum AlertType {
    Success = 1,
    Info = 2,
    Warning = 3,
    Error = 4
}

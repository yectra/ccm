import { BaseModel, DataRequest } from './base.model'

export class BillingModel {
    id: string;
    appId:string;
    secretKey:string;
    customerName: string;
    customerPhone: string;
    customerEmail: string;
    orderAmount: number;
    orderId: number;
    returnUrl: string="http://localhost:8080";
    orderNote: string;
    paymentLink:string;
    status:string;
   
}

export class BillingRequest extends DataRequest {
    search: string;
}


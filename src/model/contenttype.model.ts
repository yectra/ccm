import { BaseModel, DataRequest } from './base.model';
import { UserModel } from './user.model';

export class ContentTypeFieldModel {
    public name: string;
    public type: string;
    public oldName:String;
    public required?: boolean;
}

export class ContentTypeModel extends BaseModel {
    public id: string;
    public siteId: string;
    public name: string = "";
    public status: string;
    public fields: Array<ContentTypeFieldModel> = [];
    
    public createdBy: string;
    public createdDate: string;
    public modifiedBy: string;
    public modifiedDate: string;
}

export class ContentTypeRequest extends DataRequest {
    search: string;
}

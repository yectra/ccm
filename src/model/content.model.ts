import { BaseModel, DataRequest } from './base.model';
import { UserModel } from './user.model';

export class ContentData {
    public name: string;
    public type: string;
    public value: string;

    public controlData?: any;
}

export class ContentModel extends BaseModel {
    public id: string;
    public siteId: number;
    public name: string = "";
    public status: string;
    public contentTypeId: string;
    public data: Array<ContentData> = [];
    
    public createdBy: string;
    public createdDate: string;
    public modifiedBy: string;
    public modifiedDate: string;
}

export class ContentRequest extends DataRequest {
    search: string;
}

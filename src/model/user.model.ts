import { BaseModel, DataRequest } from './base.model'

export class UserModel {
    id: string;
    userName: string;
    fullName: string;
    firstName: string;
    lastName: string;
    email: string;
    role: string;
    status: string;
    emailVerified: boolean;
}

export class UserRequest extends DataRequest {
    search: string;
}
import { BaseModel, DataRequest } from './base.model';
import { UserModel } from './user.model';

export class SiteModel extends BaseModel {
    public id: string;
    public name: string = "";
    public status: string;
    public urls:string;
    public robots:string=" User-agent: * \n Allow: /";
    
    public createdBy: string;
    public createdDate: string;
    public modifiedBy: string;
    public modifiedDate: string;
}

export class SiteRequest extends DataRequest {
    search: string;
}

import { BaseModel, DataRequest } from './base.model';
import { UserModel } from './user.model';

export class FileModel extends BaseModel {
    public id: string;
    public siteId: string;
    public name: string = "";
    public status: string;
    public file: string;
    public fileName: string;
    
    public createdBy: string;
    public createdDate: string;
    public modifiedBy: string;
    public modifiedDate: string;
}

export class FileRequest extends DataRequest {
    search: string;
}

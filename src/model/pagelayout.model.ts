import { BaseModel, DataRequest } from './base.model';
import { UserModel } from './user.model';

export class PageLayoutSection {
    public name: string;
}

export class PageLayoutModel extends BaseModel {
    public id: string;
    public siteId: string;
    public name: string;
    public status: string;
    public sections: Array<PageLayoutSection> = [];
    
    public createdBy: string;
    public createdDate: string;
    public modifiedBy: string;
    public modifiedDate: string;
}

export class PageLayoutRequest extends DataRequest {
    search: string;
}

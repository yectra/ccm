import { GetterTree, MutationTree, ActionTree } from 'vuex';

import { ContextState } from '@/model';

const state: ContextState = {
    siteId: "",
    drawer: true
}

const getters: GetterTree<ContextState, any> = {
    siteId: state => {
        return state.siteId;
    },
    navDrawer: state => {
        return state.drawer;
    }
}

const mutations: MutationTree<ContextState> = {
    onSiteChage(state, siteId) {
        state.siteId = siteId;
    },

    onDrawerChange(state) {
        state.drawer = !state.drawer;
    }
}

const actions: ActionTree<ContextState, any> = {
    setSite(context, siteId) {
        context.commit("onSiteChage", siteId);
    },

    setNavDrawer(context) {
        context.commit("onDrawerChange");
    }
}

export const ContextModule = {
    state,
    getters,
    mutations,
    actions
}
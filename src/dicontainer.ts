import { Vue, Provide } from "vue-property-decorator";

import { IContentService, ContentService, ContentMockService } from '@/service';
import { IContentTypeService, ContentTypeService, ContentTypeMockService } from '@/service';
import { IPageService, PageService, PageMockService } from '@/service';
import { IPageLayoutService, PageLayoutService, PageLayoutMockService } from '@/service';
import { IFileService, FileService, FileMockService } from '@/service';
import { IUserService, UserService } from '@/service';
import { ISiteService, SiteService, SiteMockService } from '@/service';
import { ISubscriptionService, SubscriptionService } from '@/service';
import { IAttributeService, AttributeService,AttributeMockService } from '@/service';


export class DIContainer extends Vue {
    @Provide('contentService') contentService: IContentService = new ContentService();
    @Provide('contentTypeService') contentTypeService: IContentTypeService = new ContentTypeService();
    @Provide('pageService') pageService: IPageService = new PageService();
    @Provide('pageLayoutService') pageLayoutService: IPageLayoutService = new PageLayoutService();
    @Provide('userService') userService: IUserService = new UserService();
    @Provide('fileService') fileService: IFileService = new FileService();
    @Provide('siteService') siteService: ISiteService = new SiteService();
    @Provide('subscriptionService') subscriptionService: ISubscriptionService = new SubscriptionService();
    @Provide('attributeService') attributeService: IAttributeService = new AttributeMockService();

}
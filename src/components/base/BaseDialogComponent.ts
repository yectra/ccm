import { Component, Vue, Prop } from "vue-property-decorator";
import { IBaseModel } from "@/model";
import { BaseComponent } from './BaseComponent';

@Component
export class BaseDialogComponent<M extends IBaseModel> extends BaseComponent {
  show: boolean = false;

  dialogWidth: number = window.innerWidth - 400;

  @Prop() data: M;
  @Prop() name: string;

  open(isEdit: boolean) {
    this.show = true;
  }

  close() {
    this.show = false;

    this.$emit("close", false);
  }

  protected onSave() {
    this.show = false;

    this.$emit("close", true);
  }

}
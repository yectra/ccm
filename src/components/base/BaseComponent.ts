import { Vue } from "vue-property-decorator";
import { AxiosResponse, AxiosError } from "axios";
import { UserModel, AlertType } from '@/model';

export class BaseComponent extends Vue {
    get valid() {
        let form: any = this.$refs.form;
        return form.validate();
    }

    get userInfo(): UserModel {
        return this.$store.getters.userInfo;
    }

    error(e: AxiosError) {
        let root: any = this.$root;

        root.$alert(e.response.data.message, AlertType.Error);
    }

    alert(message: string, type: AlertType) {
        let root: any = this.$root;

        root.$alert(message, type);
    }

    confirm(title: string, message: string) {
        let root: any = this.$root;

        return root.$confirm(title, message);
    }
}
import { SubscriptionModel, SubscriptionRequest, ListItem, DataRespone, PageInfo } from '@/model';
import { IBaseService, BaseService, BaseMockService } from './base.service'

import axios from 'axios';
import { AxiosResponse, AxiosError } from 'axios'

export interface ISubscriptionService extends IBaseService<SubscriptionRequest, SubscriptionModel> {
   
}



export class SubscriptionService extends BaseService<SubscriptionRequest, SubscriptionModel> implements ISubscriptionService {
    constructor() {
        super("subscription");
    }

  
}







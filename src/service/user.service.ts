import { UserModel, UserRequest, ListItem, DataRespone, PageInfo } from '@/model';
import { IBaseService, BaseService } from './base.service'

import axios from 'axios';
import { AxiosResponse, AxiosError } from 'axios'

export interface IUserService extends IBaseService<UserRequest, UserModel> {
    getRoles(includeDefault?: boolean): Promise<Array<ListItem>>;
}

export class UserService extends BaseService<UserRequest, UserModel> implements IUserService {
    constructor() {
        super("user");
    }

    getRoles(includeDefault?: boolean): Promise<Array<ListItem>> {
        return new Promise((resolve, reject) => {
            let items = new Array<ListItem>();
            if (includeDefault) items.push(new ListItem("All Status", ""));

            items.push(new ListItem("Admin", "ADMIN"));
            items.push(new ListItem("Staff", "STAFF"));
            items.push(new ListItem("Manger", "MANAGER"));
            items.push(new ListItem("Custom", "CUSTOM"));

            resolve(items);
        });
    }
}




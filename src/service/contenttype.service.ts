import { ContentTypeModel, ContentTypeRequest, ListItem, DataRespone, PageInfo } from '@/model';
import { IBaseService, BaseService, BaseMockService } from './base.service'

import axios from 'axios';
import { AxiosResponse, AxiosError } from 'axios'

export interface IContentTypeService extends IBaseService<ContentTypeRequest, ContentTypeModel> {
    isNameExists(item: ContentTypeModel): Promise<boolean>;
    getListItems(includeDefault?: boolean): Promise<Array<ListItem>>;
}

export class ContentTypeService extends BaseService<ContentTypeRequest, ContentTypeModel> implements IContentTypeService {
    constructor() {
        super("contentType");
    }

    isNameExists(item: ContentTypeModel): Promise<boolean> {
        let request = new ContentTypeRequest();
        request._skipPaging = true;

        return this.getItems(request).then(response => {
            return response.data.some(
                p => (!item.id || item.id != p.id) && p.name.toLowerCase() == item.name.toLowerCase()
            );
        });
    }

    getListItems(includeDefault?: boolean): Promise<Array<ListItem>> {
        let request = new ContentTypeRequest();
        request._skipPaging = true;

        return this.getItems(request).then(response => {
            let items = new Array<ListItem>();
            items.push(...response.data.map(p => new ListItem(p.name, p.id.toString())));

            if (includeDefault)
                items.splice(0, 0, { text: "All Content Types", value: "" });

            return items;
        });
    }

}

export class ContentTypeMockService extends BaseMockService<ContentTypeRequest, ContentTypeModel> implements IContentTypeService {
    constructor() {
        super("contentType");
    }

    isNameExists(item: ContentTypeModel): Promise<boolean> {
        return new Promise((resolve, reject) => {
            resolve(false);
        });
    }

    getItems(request: ContentTypeRequest) : Promise<DataRespone<ContentTypeModel>> {
        return new Promise((resolve, reject) => {
            let items = new Array<ContentTypeModel>();
            let item = new ContentTypeModel();
            item.name = "Simple Content";
            item.id = "1";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            item.fields = [];
            item.fields.push({ name: "Text", type: "Editor",oldName:""});
            items.push(item);

            item = new ContentTypeModel();
            item.name = "News";
            item.id = "2";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            item.fields = [];
            item.fields.push({ name: "Title", type: "Textbox", required: true ,oldName:""});
            item.fields.push({ name: "Date", type: "DatePicker", required: true ,oldName:""});
            item.fields.push({ name: "Author", type: "Textbox",oldName:""});
            item.fields.push({ name: "Description", type: "Editor",oldName:""});
            items.push(item);

            item = new ContentTypeModel();
            item.name = "Announcement";
            item.id = "3";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            item.fields = [];
            item.fields.push({ name: "Message", type: "Textbox", required: true,oldName:"" });
            item.fields.push({ name: "Department", type: "Dropdown", required: true,oldName:"" });
            items.push(item);

            let response = new DataRespone<ContentTypeModel>();
            response.data = items;
            response.pageInfo = new PageInfo();
            response.pageInfo.page = 1;
            response.pageInfo.pageSize = 10;
            response.pageInfo.totalPages = 1;
            response.pageInfo.totalResults = items.length;

            resolve(response);
        });
    }

    getListItems(includeDefault?: boolean): Promise<Array<ListItem>> {
        let request = new ContentTypeRequest();
        request._skipPaging = true;

        return this.getItems(request).then(response => {
            let items = new Array<ListItem>();
            items.push(...response.data.map(p => new ListItem(p.name, p.id.toString())));

            if (includeDefault)
                items.splice(0, 0, { text: "All Content Types", value: "" });

            return items;
        });
    }

}
import { SiteModel, SiteRequest, ListItem, DataRespone, PageInfo } from '@/model';
import { IBaseService, BaseService, BaseMockService } from './base.service'

import axios from 'axios';
import { AxiosResponse, AxiosError } from 'axios'

export interface ISiteService extends IBaseService<SiteRequest, SiteModel> {
}

export class SiteService extends BaseService<SiteRequest, SiteModel> implements ISiteService {
    constructor() {
        super("site");
    }
}

export class SiteMockService extends BaseMockService<SiteRequest, SiteModel> implements ISiteService {
    constructor() {
        super("site");
        
    }

    getItems(request: SiteRequest) : Promise<DataRespone<SiteModel>> {
        return new Promise((resolve, reject) => {
            let items = new Array<SiteModel>();
            let item = new SiteModel();
            item.name = "Simple Site";
            item.id = "5e76df7078b1ee4de59ecbbe";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            item = new SiteModel();
            item.name = "Test Site";
            item.id = "5e76df7078b1ee4de59e6d";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            item = new SiteModel();
            item.name = "Site3";
            item.id = "5e76df7078b1ee4de4b6cbbe";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            let response = new DataRespone<SiteModel>();
            response.data = items;
            response.pageInfo = new PageInfo();
            response.pageInfo.page = 1;
            response.pageInfo.pageSize = 10;
            response.pageInfo.totalPages = 1;
            response.pageInfo.totalResults = items.length;

            resolve(response);
        });
    }

}
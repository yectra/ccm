import { PageModel, PageRequest, ListItem, DataRespone, PageInfo } from '@/model';
import { IBaseService, BaseService, BaseMockService } from './base.service'

import axios from 'axios';
import { AxiosResponse, AxiosError } from 'axios'

export interface IPageService extends IBaseService<PageRequest, PageModel> {
    
}

export class PageService extends BaseService<PageRequest, PageModel> implements IPageService {
    constructor() {
        super("page");
    }
    
}

export class PageMockService extends BaseMockService<PageRequest, PageModel> implements IPageService {
    constructor() {
        super("page");
    }

    getItems(request: PageRequest) : Promise<DataRespone<PageModel>> {
        return new Promise((resolve, reject) => {
            let items = new Array<PageModel>();
            let item = new PageModel();
            item.name = "Page 1";
            item.id = 1;
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            item = new PageModel();
            item.name = "Page 2";
            item.id = 2;
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            item = new PageModel();
            item.name = "Page 3";
            item.id = 3;
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            let response = new DataRespone<PageModel>();
            response.data = items;
            response.pageInfo = new PageInfo();
            response.pageInfo.page = 1;
            response.pageInfo.pageSize = 10;
            response.pageInfo.totalPages = 1;
            response.pageInfo.totalResults = items.length;

            resolve(response);
        });
    }

    
}
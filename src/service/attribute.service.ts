import { AttributeModel,Attributes, AttributeRequest, ListItem, DataRespone, PageInfo } from '@/model';
import { IBaseService, BaseService, BaseMockService } from './base.service'

import axios from 'axios';
import { AxiosResponse, AxiosError } from 'axios'

export interface IAttributeService extends IBaseService<AttributeRequest,AttributeModel> {
   
}



export class AttributeService extends BaseService<AttributeRequest, AttributeModel> implements IAttributeService {
    constructor() {
        super("attribute");
    }

  
}


export class AttributeMockService extends BaseMockService<AttributeRequest, AttributeModel> implements IAttributeService {
    constructor() {
        super("attribute");
    }

    getItems(request: AttributeRequest) : Promise<DataRespone<AttributeModel>> {
        return new Promise((resolve, reject) => {
            let items = new Array<AttributeModel>();
            let item = new AttributeModel();
            let attributeArray = new Array<Attributes>();
            let attribute=new Attributes();
            attribute.enum='enum';
            attribute.name='Attribute1';
            attribute.type='String';
            item.category = "Category 1";
            item.id='1';
            attributeArray.push(attribute);
            item.items=attributeArray;
            items.push(item);

         

            let response = new DataRespone<AttributeModel>();
            response.data = items;
            response.pageInfo = new PageInfo();
            response.pageInfo.page = 1;
            response.pageInfo.pageSize = 10;
            response.pageInfo.totalPages = 1;
            response.pageInfo.totalResults = items.length;

            resolve(response);
        });
    }



}




import { ContentModel, ContentRequest, ListItem, DataRespone, PageInfo } from '@/model';
import { IBaseService, BaseService, BaseMockService } from './base.service'

import axios from 'axios';
import { AxiosResponse, AxiosError } from 'axios'

export interface IContentService extends IBaseService<ContentRequest, ContentModel> {
    isNameExists(item: ContentModel): Promise<boolean>;
    getListItems(includeDefault?: boolean): Promise<Array<ListItem>>;
}

export class ContentService extends BaseService<ContentRequest, ContentModel> implements IContentService {
    constructor() {
        super("content");
    }

    getListItems(includeDefault?: boolean): Promise<Array<ListItem>> {
        let request = new ContentRequest();
        request._skipPaging = true;

        return this.getItems(request).then(response => {
            let items = new Array<ListItem>();

            items.push(...response.data.map(p => new ListItem(p.name, p.id.toString())));

            if (includeDefault)
                items.splice(0, 0, { text: "All Contents", value: "" });

            return items;
        });
    }

    isNameExists(item: ContentModel): Promise<boolean> {
        let request = new ContentRequest();
        request._skipPaging = true;

        return this.getItems(request).then(response => {
            return response.data.some(
                p => (!item.id || item.id != p.id) && p.name.toLowerCase() == item.name.toLowerCase()
            );
        });
    }
}

export class ContentMockService extends BaseMockService<ContentRequest, ContentModel> implements IContentService {
    constructor() {
        super("content");
    }

    getItems(request: ContentRequest) : Promise<DataRespone<ContentModel>> {
        return new Promise((resolve, reject) => {
            let items = new Array<ContentModel>();
            let item = new ContentModel();
            item.name = "Content 1";
            item.id = "1";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            item = new ContentModel();
            item.name = "News Content";
            item.id = "2";
            item.contentTypeId = "2";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            item = new ContentModel();
            item.name = "Announcement Content";
            item.id = "3";
            item.contentTypeId = "3";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            let response = new DataRespone<ContentModel>();
            response.data = items;
            response.pageInfo = new PageInfo();
            response.pageInfo.page = 1;
            response.pageInfo.pageSize = 10;
            response.pageInfo.totalPages = 1;
            response.pageInfo.totalResults = items.length;

            resolve(response);
        });
    }

    getListItems(includeDefault?: boolean): Promise<Array<ListItem>> {
        return new Promise((resolve, reject) => {
            resolve([]);    
        });
    }

    isNameExists(item: ContentModel): Promise<boolean> {
        return new Promise((resolve, reject) => {
            resolve(false);
        });
    }
}
import { PageLayoutModel, PageLayoutRequest, ListItem, DataRespone, PageInfo } from '@/model';
import { IBaseService, BaseService, BaseMockService } from './base.service'

import axios from 'axios';
import { AxiosResponse, AxiosError } from 'axios'

export interface IPageLayoutService extends IBaseService<PageLayoutRequest, PageLayoutModel> {
    getListItems(includeDefault?: boolean): Promise<Array<ListItem>>;
}

export class PageLayoutService extends BaseService<PageLayoutRequest, PageLayoutModel> implements IPageLayoutService {
    constructor() {
        super("pageLayout");
    }

    getListItems(includeDefault?: boolean): Promise<Array<ListItem>> {
        let request = new PageLayoutRequest();
        request._skipPaging = true;

        return this.getItems(request).then(response => {
            let items = new Array<ListItem>();
            items.push(...response.data.map(p => new ListItem(p.name, p.id.toString())));

            if (includeDefault)
                items.splice(0, 0, { text: "All Layouts", value: "" });

            return items;
        });
    }

}

export class PageLayoutMockService extends BaseMockService<PageLayoutRequest, PageLayoutModel> implements IPageLayoutService {
    constructor() {
        super("pageLayout");
    }

    getItems(request: PageLayoutRequest) : Promise<DataRespone<PageLayoutModel>> {
        return new Promise((resolve, reject) => {
            let items = new Array<PageLayoutModel>();
            let item = new PageLayoutModel();
            item.name = "Simple Layout";
            item.id = "1";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            item.sections = [];
            item.sections.push({ name: "Banner"});
            item.sections.push({ name: "About Us"});
            item.sections.push({ name: "Services"});
            item.sections.push({ name: "Careers"});
            items.push(item);

            item = new PageLayoutModel();
            item.name = "News Layout";
            item.id = "2";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            item.sections = [];
            item.sections.push({ name: "Section1" });
            items.push(item);


            let response = new DataRespone<PageLayoutModel>();
            response.data = items;
            response.pageInfo = new PageInfo();
            response.pageInfo.page = 1;
            response.pageInfo.pageSize = 10;
            response.pageInfo.totalPages = 1;
            response.pageInfo.totalResults = items.length;

            resolve(response);
        });
    }

    getListItems(includeDefault?: boolean): Promise<Array<ListItem>> {
        let request = new PageLayoutRequest();
        request._skipPaging = true;

        return this.getItems(request).then(response => {
            let items = new Array<ListItem>();
            items.push(...response.data.map(p => new ListItem(p.name, p.id.toString())));

            if (includeDefault)
                items.splice(0, 0, { text: "All Layouts", value: "" });

            return items;
        });
    }

}
import { FileModel, FileRequest, ListItem, DataRespone, PageInfo } from '@/model';
import { IBaseService, BaseService, BaseMockService } from './base.service'

import axios from 'axios';
import { AxiosResponse, AxiosError } from 'axios'

export interface IFileService extends IBaseService<FileRequest, FileModel> {
    uploadPhoto(file: File, fileId: string): Promise<any>;
}

export class FileService extends BaseService<FileRequest, FileModel> implements IFileService {
    constructor() {
        super("file");
    }

    
    uploadPhoto(file: File, fileId: string): Promise<any> {
        let formData = new FormData();
        formData.append("uploadImage", file);
        return this.upload(formData, `${fileId}/uploadFile`);
    }
}

export class FileMockService extends BaseMockService<FileRequest, FileModel> implements IFileService {
    constructor() {
        super("file");
    }

    getItems(request: FileRequest) : Promise<DataRespone<FileModel>> {
        return new Promise((resolve, reject) => {
            let items = new Array<FileModel>();
            let item = new FileModel();
            item.name = "Simple File";
            item.id = "1";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            item = new FileModel();
            item.name = "News";
            item.id = "2";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            item = new FileModel();
            item.name = "Announcement";
            item.id = "3";
            item.createdBy = "Kumar R"
            item.createdDate = (new Date()).toDateString();
            items.push(item);

            let response = new DataRespone<FileModel>();
            response.data = items;
            response.pageInfo = new PageInfo();
            response.pageInfo.page = 1;
            response.pageInfo.pageSize = 10;
            response.pageInfo.totalPages = 1;
            response.pageInfo.totalResults = items.length;

            resolve(response);
        });
    }
    uploadPhoto(file: File, fileId: string): Promise<any> {
        let formData = new FormData();
        formData.append("uploadImage", file);

        return this.upload(formData, `${fileId}/uploadAvatar`);
    }

}
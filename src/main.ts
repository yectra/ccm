import Vue from 'vue';
import VueMoment from 'vue-moment';

import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import './plugins/base';
import './plugins/chartist';
import VueHelper from './plugins/vue-helper';
import App from './App.vue';

class AppBootstrap {
  constructor() {
    Vue.config.productionTip = false;

    Vue.use(VueMoment);
    Vue.use(VueHelper);   

    new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app');
  }
}



new AppBootstrap()




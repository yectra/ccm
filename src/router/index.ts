import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';
import axios from "axios";

Vue.use(VueRouter);

const routes = [
  {
    path: '/logout',
    name: 'logout',
    component: () => import('@/views/general/Logout.vue'),
    meta: { anonymous: true }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/general/Index.vue'),
    meta: { anonymous: true }
  },
  {
    path: '/accessdenied',
    name: 'accessdenied',
    component: () => import('@/views/general/AccessDenied.vue'),
    meta: { anonymous: true }
  },
  {
    path: '/',
    component: () => import('@/views/pages/Index.vue'),
    children: [
      {
        path: '',
        name: 'dashboard',
        component: () => import('@/views/pages/dashboard/Index.vue')
      },
      {
        path: 'contents',
        name: 'content',
        component: () => import('@/views/pages/content/Index.vue')
      },
      {
        path: 'contenttypes',
        name: 'contenttype',
        component: () => import('@/views/pages/contenttype/Index.vue')
      },
      {
        path: 'pages',
        name: 'page',
        component: () => import('@/views/pages/page/Index.vue')
      },
      {
        path: 'pagelayout',
        name: 'pagelayout',
        component: () => import('@/views/pages/pagelayout/Index.vue')
      },
      {
        path: 'files',
        name: 'file',
        component: () => import('@/views/pages/file/Index.vue')
      },
      {
        path: 'sites',
        name: 'site',
        component: () => import('@/views/pages/site/Index.vue')
      },
      {
        path: 'users',
        name: 'user',
        component: () => import('@/views/pages/user/Index.vue')
      },
      {
        path: 'subscription',
        name: 'subscription',
        component: () => import('@/views/pages/subscription/Index.vue')
      },
      {
        path: 'attributes',
        name: 'attributes',
        component: () => import('@/views/pages/attributes/Index.vue')
      }
    ]
  }
];

const router = new VueRouter({
  mode: 'history',
  routes: routes
});

export default router;

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => !record.meta.anonymous)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }

    store.dispatch("login").then(() => {
      if (store.getters.isLoggedIn) {

        let token = store.getters.accessToken;
        if (token)
          axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;

        next("/")
      }
      else
        next("/accessdenied")
    });

  } else {
    next()
  }
});